# Automation script for AWS CIS Foundations Benchmark Audits

## Problem Statement:
For every AWS Infrastructure Security Testing project we are required to run a set of AWS commands and analyse the output. This process has been manual so far. By automating the manual work involved, we could save considerable amount of time and effort. 

## Solution:
A Golang code `aws_cis_audit.go` will run all the required AWS commands (that are part of our AWS Infrastructure Security Testing projects) without any manual intervention. The results of the command runs will be stored in an output file named as `out.txt`. The output is well formatted and makes analysis easier. The entire process, as described above, takes less than 3 minutes.

## How to use:
1. Download [aws_cis_audit.go](aws_cis_audit.go) file
2. Configure AWS CLI
3. Open the aws_cis_audit.go file and change `const profile string = ""` to `const profile string = " --profile profileNameHere"` in case you want to use an AWS profile
4. Run the command: `go run aws_cis_audit.go`
5. Analyse `out.txt` file
