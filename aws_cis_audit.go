package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

// const profile string = " --profile training"
const profile string = ""

const iam1 string = "aws iam generate-credential-report"
const iam2 string = "aws iam get-credential-report --query 'Content' --output text"
const iam2a string = " | base64 -d | cut -d, -f1,5,11,16 | grep -B1 '<root_account>'"
const iam2b string = " | base64 -d | cut -d, -f1,4,8"
const iam2c string = " | base64 -d | cut -d, -f1,4,5,6,9,10,11,14,15,16"
const iam2d string = " | base64 -d"
const iam6 string = "aws iam get-account-password-policy"
const iam8 string = "aws iam get-account-summary"
const iam8a string = " | grep \"AccountMFAEnabled\""
const iam9 string = "aws iam list-virtual-mfa-devices"
const iam10 string = "aws iam list-users --query 'Users[*].UserName'"
const iam11 string = "aws iam list-attached-user-policies --user-name " //<iam_user>
const iam12 string = "aws iam list-user-policies --user-name "          //<iam_user>
const iam13 string = "aws iam list-policies --query \"Policies[?PolicyName == 'AWSSupportAccess']\""
const iam14 string = "aws iam list-entities-for-policy --policy-arn arn:aws:iam::aws:policy/AWSSupportAccess"
const iam15 string = "aws iam list-policies --only-attached --query \"Policies[*].{Arn:Arn,Version:DefaultVersionId}\" --output table"
const iam16 string = "aws iam get-policy-version --policy-arn \"arn:aws:iam::aws:policy/AdministratorAccess\" --version-id v1"
const iam17 string = "aws iam list-entities-for-policy --policy-arn \"arn:aws:iam::aws:policy/AdministratorAccess\""
const iam18 string = "aws configservice describe-configuration-recorders"
const ec2reg string = "aws ec2 describe-regions --output text --query 'Regions[*].[RegionName]'"
const ec2ins string = "aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region " //<region-name>
const ec2iam string = "aws ec2 describe-iam-instance-profile-associations --output json --region "                             //<region-name>
const ec2vpc string = "aws ec2 describe-vpcs --query 'Vpcs[*].{VpcId: VpcId}' "
const ec2routePrefix string = "aws ec2 describe-route-tables --filter \"Name=vpc-id,Values=" //vpc-911fc7ea
const ec2routeSuffix string = "\" --query \"RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}\""

const cloudtrail1 string = "aws cloudtrail describe-trails"
const cloudtrail2 string = "aws cloudtrail get-event-selectors --trail-name "                             //<TRAIL_NAME_HERE>
const cloudtrail3 string = "aws cloudtrail describe-trails --region "                                     //<REGION-NAME>
const cloudtrail4 string = "aws cloudtrail describe-trails --query 'trailList[*].S3BucketName' --region " //<REGION-NAME>                                   //<REGION-NAME>

const s3api1 string = "aws s3api get-bucket-acl --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AllUsers`]' --bucket "           //<BUCKET_NAME_HERE>
const s3api2 string = "aws s3api get-bucket-acl --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AuthenticatedUsers`]' --bucket " //<BUCKET_NAME_HERE>
const s3api3 string = "aws s3api get-bucket-policy --bucket "                                                                                           //<BUCKET_NAME_HERE>
const s3api4 string = "aws s3api get-bucket-logging --bucket "                                                                                          //<BUCKET_NAME_HERE>

//Cloudtrail will store trail name and region information
type Cloudtrail struct {
	trailnames []string
	regions    []string
	isset      bool
}

var outfile, err = os.Create("")
var writer = bufio.NewWriter(outfile)

var users = make([]string, 5)
var regions = make([]string, 5)
var vpcids = make([]string, 5)
var s3bucketnames = make([]string, 5)

var cloudtrail Cloudtrail

//FetchS3BucketNames will fetch the name of S3 buckets
func FetchS3BucketNames(commandOutput string) {
	s3bucketnamesrawString := commandOutput
	temp := strings.Fields(s3bucketnamesrawString)

	s3bucketnames = nil
	for _, s3bucketname := range temp {
		re := regexp.MustCompile("\"(.*?)\"")
		match := re.FindStringSubmatch(s3bucketname)
		if match != nil {
			s3bucketname = match[1]
			s3bucketnames = append(s3bucketnames, s3bucketname)
		}
	}

	fmt.Println(s3bucketnames)
}

//FetchCloudtrailNameAndRegion will store all region names
func FetchCloudtrailNameAndRegion(commandOutput string) {
	trailnamesRawString := commandOutput
	temp := strings.Fields(trailnamesRawString)

	cloudtrail.trailnames = nil
	cloudtrail.regions = nil
	for _, trailname := range temp {
		if strings.Contains(trailname, "arn") {
			//Extract Cloudtrail Name. Remove surrounding quotes.
			re := regexp.MustCompile("\"(.*?)\"")
			match := re.FindStringSubmatch(trailname)
			trailname = match[1]

			cloudtrail.trailnames = append(cloudtrail.trailnames[:], trailname)

			//Extract Region
			splitByColon := strings.Split(trailname, ":")
			cloudtrail.regions = append(cloudtrail.regions[:], splitByColon[3])
		}
	}

	cloudtrail.isset = true
	fmt.Println(cloudtrail)
}

//FetchVPCID will store all VPC IDs
func FetchVPCID(commandOutput string) {
	vpcidsRawString := commandOutput
	temp := strings.Fields(vpcidsRawString)

	vpcids = nil
	for _, vpcid := range temp {
		if strings.Contains(vpcid, "vpc-") {
			//Extract VPC ID. Remove surrounding quotes.
			vpcid = strings.TrimLeft(strings.TrimRight(vpcid, "\""), "\"")
			vpcids = append(vpcids[:], vpcid)
		}
	}

	fmt.Println(vpcids)
}

//FetchRegions will store all region names
func FetchRegions(commandOutput string) {
	regions = nil
	regionsRawString := commandOutput

	regions = strings.Fields(regionsRawString)
	fmt.Println(regions)
}

//FetchUsernames will store all usernames
func FetchUsernames(commandOutput string) {
	users = nil
	usersRawString := commandOutput

	json.Unmarshal([]byte(usersRawString), &users)
	fmt.Println(users)
}

//RunCommand will run AWS commands
func RunCommand(command2run string) {
	println(command2run)
	go io.Copy(writer, strings.NewReader("\n"+command2run+"\n"))
	cmd := exec.Command("sh", "-c", command2run)

	stdoutPipe, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	err = cmd.Start()
	if err != nil {
		panic(err)
	}

	var buf bytes.Buffer
	io.Copy(&buf, stdoutPipe)
	commandOutput := buf.String()

	writer.WriteString(commandOutput)

	words := strings.Fields(command2run)
	for _, word := range words {

		switch word {
		case "list-users":
			FetchUsernames(commandOutput)
		case "describe-regions":
			FetchRegions(commandOutput)
		case "describe-vpcs":
			FetchVPCID(commandOutput)
		case "describe-trails":
			if !cloudtrail.isset {
				FetchCloudtrailNameAndRegion(commandOutput)
			}
			if strings.Contains(command2run, "S3BucketName") {
				FetchS3BucketNames(commandOutput)
			}
		}
	}

	cmd.Wait()
}

func main() {
	cloudtrail.isset = false

	// open the out file for writing
	outfile, err = os.Create("./out.txt")
	if err != nil {
		panic(err)
	}
	defer outfile.Close()

	writer = bufio.NewWriter(outfile)
	defer writer.Flush()

	RunCommand(iam1 + profile)
	RunCommand(iam2 + profile)
	RunCommand(iam2 + profile + iam2a)
	RunCommand(iam2 + profile + iam2b)
	RunCommand(iam2 + profile + iam2c)
	RunCommand(iam2 + profile + iam2d)
	RunCommand(iam6 + profile)
	RunCommand(iam8 + profile + iam8a)
	RunCommand(iam9 + profile)

	//aws iam list-users --query 'Users[*].UserName' --profile training
	RunCommand(iam10 + profile)

	if users != nil {
		for _, user := range users {
			//aws iam list-attached-user-policies --user-name <iam_user> --profile training
			RunCommand(iam11 + user + profile)
			//aws iam list-user-policies --user-name <iam_user> --profile training
			RunCommand(iam12 + user + profile)
		}
	}

	RunCommand(iam13 + profile)
	RunCommand(iam14 + profile)
	RunCommand(iam15 + profile)
	RunCommand(iam16 + profile)
	RunCommand(iam17 + profile)
	RunCommand(iam18 + profile)

	//aws ec2 describe-regions --output text --query 'Regions[*].[RegionName]'
	RunCommand(ec2reg + profile)

	if regions != nil {
		for _, region := range regions {
			//aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region <region-name>"
			RunCommand(ec2ins + region + profile)
			//aws ec2 describe-iam-instance-profile-associations --region <region-name> --output json
			RunCommand(ec2iam + region + profile)
		}
	}

	//const ec2vpc string = "aws ec2 describe-vpcs --query 'Vpcs[*].{VpcId: VpcId}' "
	RunCommand(ec2vpc + profile)

	if vpcids != nil {
		for _, vpcid := range vpcids {
			//const ec2route string = "aws ec2 describe-route-tables --filter \"Name=vpc-id,Values=vpc-911fc7ea\" --query \"RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}\""
			RunCommand(ec2routePrefix + vpcid + ec2routeSuffix + profile)
		}
	}

	// const cloudtrail1 string = "aws cloudtrail describe-trails"
	RunCommand(cloudtrail1 + profile)
	// RunCommand(cloudtrail1)

	if cloudtrail.trailnames != nil {
		for _, trailname := range cloudtrail.trailnames {
			// const cloudtrail2 string = "aws cloudtrail get-event-selectors --trail-name " //< TRAIL_NAME_HERE >
			RunCommand(cloudtrail2 + trailname + profile)
		}
	}

	if cloudtrail.regions != nil {
		for _, cloudtrailregion := range cloudtrail.regions {
			// const cloudtrail3 string = "aws cloudtrail describe-trails --region " //<REGION-NAME>
			RunCommand(cloudtrail3 + cloudtrailregion + profile)
			// const cloudtrail4 string = "aws cloudtrail describe-trails --query 'trailList[*].S3BucketName' --region " //<REGION-NAME>
			RunCommand(cloudtrail4 + cloudtrailregion + profile)
		}
	}

	if s3bucketnames != nil {
		for _, s3bucketname := range s3bucketnames {
			// const s3api1 string = "aws s3api get-bucket-acl --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AllUsers`]' --bucket "           //<BUCKET_NAME_HERE>                                                                                         //<BUCKET_NAME_HERE>
			RunCommand(s3api1 + s3bucketname + profile)
			// const s3api2 string = "aws s3api get-bucket-acl --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AuthenticatedUsers`]' --bucket " //<BUCKET_NAME_HERE>
			RunCommand(s3api2 + s3bucketname + profile)
			// const s3api3 string = "aws s3api get-bucket-policy --bucket "                                                                                           //<BUCKET_NAME_HERE>
			RunCommand(s3api3 + s3bucketname + profile)
			// const s3api4 string = "aws s3api get-bucket-logging --bucket "
			RunCommand(s3api4 + s3bucketname + profile)
		}
	}
}
