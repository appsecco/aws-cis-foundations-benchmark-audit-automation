
aws iam generate-credential-report
{
    "State": "STARTED",
    "Description": "No report exists. Starting a new report generation task"
}

aws iam get-credential-report --query 'Content' --output text
dXNlcixhcm4sdXNlcl9jcmVhdGlvbl90aW1lLHBhc3N3b3JkX2VuYWJsZWQscGFzc3dvcmRfbGFzdF91c2VkLHBhc3N3b3JkX2xhc3RfY2hhbmdlZCxwYXNzd29yZF9uZXh0X3JvdGF0aW9uLG1mYV9hY3RpdmUsYWNjZXNzX2tleV8xX2FjdGl2ZSxhY2Nlc3Nfa2V5XzFfbGFzdF9yb3RhdGVkLGFjY2Vzc19rZXlfMV9sYXN0X3VzZWRfZGF0ZSxhY2Nlc3Nfa2V5XzFfbGFzdF91c2VkX3JlZ2lvbixhY2Nlc3Nfa2V5XzFfbGFzdF91c2VkX3NlcnZpY2UsYWNjZXNzX2tleV8yX2FjdGl2ZSxhY2Nlc3Nfa2V5XzJfbGFzdF9yb3RhdGVkLGFjY2Vzc19rZXlfMl9sYXN0X3VzZWRfZGF0ZSxhY2Nlc3Nfa2V5XzJfbGFzdF91c2VkX3JlZ2lvbixhY2Nlc3Nfa2V5XzJfbGFzdF91c2VkX3NlcnZpY2UsY2VydF8xX2FjdGl2ZSxjZXJ0XzFfbGFzdF9yb3RhdGVkLGNlcnRfMl9hY3RpdmUsY2VydF8yX2xhc3Rfcm90YXRlZAo8cm9vdF9hY2NvdW50Pixhcm46YXdzOmlhbTo6MTU5MjM2MTY0NzM0OnJvb3QsMjAxOC0wMi0xNVQxMDowMzoxNyswMDowMCxub3Rfc3VwcG9ydGVkLDIwMjAtMDEtMTZUMTA6MzQ6MjArMDA6MDAsbm90X3N1cHBvcnRlZCxub3Rfc3VwcG9ydGVkLHRydWUsdHJ1ZSwyMDE4LTA1LTAyVDAxOjM5OjMxKzAwOjAwLE4vQSxOL0EsTi9BLGZhbHNlLE4vQSxOL0EsTi9BLE4vQSxmYWxzZSxOL0EsZmFsc2UsTi9BCmFiaGlzZWssYXJuOmF3czppYW06OjE1OTIzNjE2NDczNDp1c2VyL2FiaGlzZWssMjAxOS0wOC0yMlQwOTo0MzoyOCswMDowMCx0cnVlLDIwMjAtMDgtMDRUMDc6NTU6MzArMDA6MDAsMjAyMC0wMi0yNVQwNzozNDo1NiswMDowMCwyMDIwLTA4LTIzVDA3OjM0OjU2KzAwOjAwLHRydWUsdHJ1ZSwyMDE5LTA4LTIyVDA5OjUyOjI3KzAwOjAwLDIwMjAtMDgtMDdUMDU6MzI6MDArMDA6MDAsdXMtZWFzdC0xLHJvdXRlNTMsZmFsc2UsTi9BLE4vQSxOL0EsTi9BLGZhbHNlLE4vQSxmYWxzZSxOL0EKYWthc2gsYXJuOmF3czppYW06OjE1OTIzNjE2NDczNDp1c2VyL2FrYXNoLDIwMTgtMDItMTZUMDU6NTI6MDQrMDA6MDAsdHJ1ZSwyMDE5LTA4LTI4VDA3OjAzOjAyKzAwOjAwLDIwMTktMDQtMjRUMTU6NTI6NTArMDA6MDAsMjAxOS0xMC0yMVQxNTo1Mjo1MCswMDowMCx0cnVlLHRydWUsMjAxOS0wNC0yNFQxNTo1MDo1NyswMDowMCwyMDIwLTA1LTEyVDA3OjM5OjAwKzAwOjAwLHVzLWVhc3QtMSxzdHMsZmFsc2UsTi9BLE4vQSxOL0EsTi9BLGZhbHNlLE4vQSxmYWxzZSxOL0EKYXdzLXRyYWluaW5nLGFybjphd3M6aWFtOjoxNTkyMzYxNjQ3MzQ6dXNlci9hd3MtdHJhaW5pbmcsMjAyMC0wMy0wM1QwNDo0NDowOSswMDowMCx0cnVlLG5vX2luZm9ybWF0aW9uLDIwMjAtMDMtMDNUMDQ6NDQ6MTIrMDA6MDAsMjAyMC0wOC0zMFQwNDo0NDoxMiswMDowMCxmYWxzZSx0cnVlLDIwMjAtMDMtMDNUMDQ6NDQ6MTErMDA6MDAsTi9BLE4vQSxOL0EsZmFsc2UsTi9BLE4vQSxOL0EsTi9BLGZhbHNlLE4vQSxmYWxzZSxOL0EKYmhhcmF0aCxhcm46YXdzOmlhbTo6MTU5MjM2MTY0NzM0OnVzZXIvYmhhcmF0aCwyMDE4LTAyLTE5VDExOjQzOjU3KzAwOjAwLHRydWUsMjAyMC0wOC0wN1QwNjo0MzozOCswMDowMCwyMDIwLTAyLTI2VDA4OjE3OjA2KzAwOjAwLDIwMjAtMDgtMjRUMDg6MTc6MDYrMDA6MDAsZmFsc2UsZmFsc2UsMjAxOS0wOS0yNFQyMzo0ODowMCswMDowMCxOL0EsTi9BLE4vQSx0cnVlLDIwMjAtMDItMjZUMDg6MTU6MjgrMDA6MDAsMjAyMC0wOC0wOFQxMzowMDowMCswMDowMCx1cy1lYXN0LTEsc3RzLGZhbHNlLE4vQSxmYWxzZSxOL0EKY3RmMy1jaGFsbGVuZ2UsYXJuOmF3czppYW06OjE1OTIzNjE2NDczNDp1c2VyL2N0ZjMtY2hhbGxlbmdlLDIwMTgtMDItMTlUMTM6MTU6NDkrMDA6MDAsZmFsc2UsTi9BLE4vQSxOL0EsZmFsc2UsZmFsc2UsMjAxOC0wMi0xOVQxMzoxNTo1MSswMDowMCwyMDIwLTA4LTA4VDIyOjQyOjAwKzAwOjAwLHVzLWVhc3QtMSxzMyxmYWxzZSxOL0EsTi9BLE4vQSxOL0EsZmFsc2UsTi9BLGZhbHNlLE4vQQpyaXlheixhcm46YXdzOmlhbTo6MTU5MjM2MTY0NzM0OnVzZXIvcml5YXosMjAxOC0wMi0xNlQwNTo1MjowNCswMDowMCx0cnVlLDIwMjAtMDgtMDlUMTI6MzU6MjUrMDA6MDAsMjAyMC0wOC0wOFQwODoyNzo1NSswMDowMCwyMDIxLTAyLTA0VDA4OjI3OjU1KzAwOjAwLGZhbHNlLHRydWUsMjAxOC0wMi0xNlQwNTo1MjowNSswMDowMCwyMDIwLTA4LTA4VDIyOjE1OjAwKzAwOjAwLHVzLWVhc3QtMSxzMyx0cnVlLDIwMTktMDktMjRUMjM6NTE6NDUrMDA6MDAsMjAxOS0xMS0xM1QwNTo1MjowMCswMDowMCx1cy1lYXN0LTEsczMsZmFsc2UsTi9BLGZhbHNlLE4vQQpzZWN1cml0eWF1ZGl0dXNlcixhcm46YXdzOmlhbTo6MTU5MjM2MTY0NzM0OnVzZXIvc2VjdXJpdHlhdWRpdHVzZXIsMjAyMC0wOC0wNFQxNjowMjoyMiswMDowMCxmYWxzZSxOL0EsTi9BLE4vQSxmYWxzZSx0cnVlLDIwMjAtMDgtMDRUMTY6MDI6MjMrMDA6MDAsMjAyMC0wOC0wOVQxNzoyODowMCswMDowMCx1cy1lYXN0LTEsY2xvdWR0cmFpbCxmYWxzZSxOL0EsTi9BLE4vQSxOL0EsZmFsc2UsTi9BLGZhbHNlLE4vQQpzdGFydHJla2J1dHRlcixhcm46YXdzOmlhbTo6MTU5MjM2MTY0NzM0OnVzZXIvc3RhcnRyZWtidXR0ZXIsMjAyMC0wOC0wOFQyMTozNzozMiswMDowMCxmYWxzZSxOL0EsTi9BLE4vQSxmYWxzZSx0cnVlLDIwMjAtMDgtMDhUMjE6Mzc6MzQrMDA6MDAsMjAyMC0wOC0wOVQxMzowMzowMCswMDowMCx1cy1lYXN0LTEsczMsZmFsc2UsTi9BLE4vQSxOL0EsTi9BLGZhbHNlLE4vQSxmYWxzZSxOL0E=

aws iam get-credential-report --query 'Content' --output text | base64 -d | cut -d, -f1,5,11,16 | grep -B1 '<root_account>'
user,password_last_used,access_key_1_last_used_date,access_key_2_last_used_date
<root_account>,2020-01-16T10:34:20+00:00,N/A,N/A

aws iam get-credential-report --query 'Content' --output text | base64 -d | cut -d, -f1,4,8
user,password_enabled,mfa_active
<root_account>,not_supported,true
abhisek,true,true
akash,true,true
aws-training,true,false
bharath,true,false
ctf3-challenge,false,false
riyaz,true,false
securityaudituser,false,false
startrekbutter,false,false

aws iam get-credential-report --query 'Content' --output text | base64 -d | cut -d, -f1,4,5,6,9,10,11,14,15,16
user,password_enabled,password_last_used,password_last_changed,access_key_1_active,access_key_1_last_rotated,access_key_1_last_used_date,access_key_2_active,access_key_2_last_rotated,access_key_2_last_used_date
<root_account>,not_supported,2020-01-16T10:34:20+00:00,not_supported,true,2018-05-02T01:39:31+00:00,N/A,false,N/A,N/A
abhisek,true,2020-08-04T07:55:30+00:00,2020-02-25T07:34:56+00:00,true,2019-08-22T09:52:27+00:00,2020-08-07T05:32:00+00:00,false,N/A,N/A
akash,true,2019-08-28T07:03:02+00:00,2019-04-24T15:52:50+00:00,true,2019-04-24T15:50:57+00:00,2020-05-12T07:39:00+00:00,false,N/A,N/A
aws-training,true,no_information,2020-03-03T04:44:12+00:00,true,2020-03-03T04:44:11+00:00,N/A,false,N/A,N/A
bharath,true,2020-08-07T06:43:38+00:00,2020-02-26T08:17:06+00:00,false,2019-09-24T23:48:00+00:00,N/A,true,2020-02-26T08:15:28+00:00,2020-08-08T13:00:00+00:00
ctf3-challenge,false,N/A,N/A,false,2018-02-19T13:15:51+00:00,2020-08-08T22:42:00+00:00,false,N/A,N/A
riyaz,true,2020-08-09T12:35:25+00:00,2020-08-08T08:27:55+00:00,true,2018-02-16T05:52:05+00:00,2020-08-08T22:15:00+00:00,true,2019-09-24T23:51:45+00:00,2019-11-13T05:52:00+00:00
securityaudituser,false,N/A,N/A,true,2020-08-04T16:02:23+00:00,2020-08-09T17:28:00+00:00,false,N/A,N/A
startrekbutter,false,N/A,N/A,true,2020-08-08T21:37:34+00:00,2020-08-09T13:03:00+00:00,false,N/A,N/A

aws iam get-credential-report --query 'Content' --output text | base64 -d
user,arn,user_creation_time,password_enabled,password_last_used,password_last_changed,password_next_rotation,mfa_active,access_key_1_active,access_key_1_last_rotated,access_key_1_last_used_date,access_key_1_last_used_region,access_key_1_last_used_service,access_key_2_active,access_key_2_last_rotated,access_key_2_last_used_date,access_key_2_last_used_region,access_key_2_last_used_service,cert_1_active,cert_1_last_rotated,cert_2_active,cert_2_last_rotated
<root_account>,arn:aws:iam::159236164734:root,2018-02-15T10:03:17+00:00,not_supported,2020-01-16T10:34:20+00:00,not_supported,not_supported,true,true,2018-05-02T01:39:31+00:00,N/A,N/A,N/A,false,N/A,N/A,N/A,N/A,false,N/A,false,N/A
abhisek,arn:aws:iam::159236164734:user/abhisek,2019-08-22T09:43:28+00:00,true,2020-08-04T07:55:30+00:00,2020-02-25T07:34:56+00:00,2020-08-23T07:34:56+00:00,true,true,2019-08-22T09:52:27+00:00,2020-08-07T05:32:00+00:00,us-east-1,route53,false,N/A,N/A,N/A,N/A,false,N/A,false,N/A
akash,arn:aws:iam::159236164734:user/akash,2018-02-16T05:52:04+00:00,true,2019-08-28T07:03:02+00:00,2019-04-24T15:52:50+00:00,2019-10-21T15:52:50+00:00,true,true,2019-04-24T15:50:57+00:00,2020-05-12T07:39:00+00:00,us-east-1,sts,false,N/A,N/A,N/A,N/A,false,N/A,false,N/A
aws-training,arn:aws:iam::159236164734:user/aws-training,2020-03-03T04:44:09+00:00,true,no_information,2020-03-03T04:44:12+00:00,2020-08-30T04:44:12+00:00,false,true,2020-03-03T04:44:11+00:00,N/A,N/A,N/A,false,N/A,N/A,N/A,N/A,false,N/A,false,N/A
bharath,arn:aws:iam::159236164734:user/bharath,2018-02-19T11:43:57+00:00,true,2020-08-07T06:43:38+00:00,2020-02-26T08:17:06+00:00,2020-08-24T08:17:06+00:00,false,false,2019-09-24T23:48:00+00:00,N/A,N/A,N/A,true,2020-02-26T08:15:28+00:00,2020-08-08T13:00:00+00:00,us-east-1,sts,false,N/A,false,N/A
ctf3-challenge,arn:aws:iam::159236164734:user/ctf3-challenge,2018-02-19T13:15:49+00:00,false,N/A,N/A,N/A,false,false,2018-02-19T13:15:51+00:00,2020-08-08T22:42:00+00:00,us-east-1,s3,false,N/A,N/A,N/A,N/A,false,N/A,false,N/A
riyaz,arn:aws:iam::159236164734:user/riyaz,2018-02-16T05:52:04+00:00,true,2020-08-09T12:35:25+00:00,2020-08-08T08:27:55+00:00,2021-02-04T08:27:55+00:00,false,true,2018-02-16T05:52:05+00:00,2020-08-08T22:15:00+00:00,us-east-1,s3,true,2019-09-24T23:51:45+00:00,2019-11-13T05:52:00+00:00,us-east-1,s3,false,N/A,false,N/A
securityaudituser,arn:aws:iam::159236164734:user/securityaudituser,2020-08-04T16:02:22+00:00,false,N/A,N/A,N/A,false,true,2020-08-04T16:02:23+00:00,2020-08-09T17:28:00+00:00,us-east-1,cloudtrail,false,N/A,N/A,N/A,N/A,false,N/A,false,N/A
startrekbutter,arn:aws:iam::159236164734:user/startrekbutter,2020-08-08T21:37:32+00:00,false,N/A,N/A,N/A,false,true,2020-08-08T21:37:34+00:00,2020-08-09T13:03:00+00:00,us-east-1,s3,false,N/A,N/A,N/A,N/A,false,N/A,false,N/A
aws iam get-account-password-policy
{
    "PasswordPolicy": {
        "AllowUsersToChangePassword": true,
        "HardExpiry": false,
        "RequireLowercaseCharacters": true,
        "MinimumPasswordLength": 20,
        "RequireNumbers": true,
        "RequireUppercaseCharacters": true,
        "ExpirePasswords": true,
        "RequireSymbols": true,
        "MaxPasswordAge": 180,
        "PasswordReusePrevention": 10
    }
}

aws iam get-account-summary | grep "AccountMFAEnabled"
        "AccountMFAEnabled": 1,

aws iam list-virtual-mfa-devices
{
    "VirtualMFADevices": [
        {
            "SerialNumber": "arn:aws:iam::159236164734:mfa/root-account-mfa-device",
            "User": {
                "UserName": "appsecco-aws-training",
                "PasswordLastUsed": "2020-01-16T10:34:20Z",
                "UserId": "159236164734",
                "Arn": "arn:aws:iam::159236164734:root",
                "CreateDate": "2018-02-15T10:03:17Z"
            },
            "EnableDate": "2018-02-16T05:58:36Z"
        },
        {
            "SerialNumber": "arn:aws:iam::159236164734:mfa/akash",
            "User": {
                "PasswordLastUsed": "2019-08-28T07:03:02Z",
                "Path": "/",
                "Arn": "arn:aws:iam::159236164734:user/akash",
                "UserName": "akash",
                "CreateDate": "2018-02-16T05:52:04Z",
                "UserId": "AIDAIE3U32YWQY5ONH7QI"
            },
            "EnableDate": "2019-04-24T15:50:37Z"
        },
        {
            "SerialNumber": "arn:aws:iam::159236164734:mfa/abhisek",
            "User": {
                "PasswordLastUsed": "2020-08-04T07:55:30Z",
                "Path": "/",
                "Arn": "arn:aws:iam::159236164734:user/abhisek",
                "UserName": "abhisek",
                "CreateDate": "2019-08-22T09:43:28Z",
                "UserId": "AIDASKEZXBR7GLPCQIILO"
            },
            "EnableDate": "2019-08-22T09:52:06Z"
        }
    ]
}

aws iam list-users --query 'Users[*].UserName'
[
    "abhisek",
    "akash",
    "aws-training",
    "bharath",
    "ctf3-challenge",
    "riyaz",
    "securityaudituser",
    "startrekbutter"
]

aws iam list-attached-user-policies --user-name abhisek
{
    "AttachedPolicies": []
}

aws iam list-user-policies --user-name abhisek
{
    "PolicyNames": []
}

aws iam list-attached-user-policies --user-name akash
{
    "AttachedPolicies": []
}

aws iam list-user-policies --user-name akash
{
    "PolicyNames": []
}

aws iam list-attached-user-policies --user-name aws-training
{
    "AttachedPolicies": [
        {
            "PolicyArn": "arn:aws:iam::aws:policy/AdministratorAccess",
            "PolicyName": "AdministratorAccess"
        }
    ]
}

aws iam list-user-policies --user-name aws-training
{
    "PolicyNames": []
}

aws iam list-attached-user-policies --user-name bharath
{
    "AttachedPolicies": [
        {
            "PolicyArn": "arn:aws:iam::aws:policy/AdministratorAccess",
            "PolicyName": "AdministratorAccess"
        }
    ]
}

aws iam list-user-policies --user-name bharath
{
    "PolicyNames": []
}

aws iam list-attached-user-policies --user-name ctf3-challenge
{
    "AttachedPolicies": [
        {
            "PolicyArn": "arn:aws:iam::159236164734:policy/ctf3-challenge-s3-bucket-policy",
            "PolicyName": "ctf3-challenge-s3-bucket-policy"
        }
    ]
}

aws iam list-user-policies --user-name ctf3-challenge
{
    "PolicyNames": []
}

aws iam list-attached-user-policies --user-name riyaz
{
    "AttachedPolicies": [
        {
            "PolicyArn": "arn:aws:iam::aws:policy/AdministratorAccess",
            "PolicyName": "AdministratorAccess"
        },
        {
            "PolicyArn": "arn:aws:iam::aws:policy/IAMUserChangePassword",
            "PolicyName": "IAMUserChangePassword"
        }
    ]
}

aws iam list-user-policies --user-name riyaz
{
    "PolicyNames": []
}

aws iam list-attached-user-policies --user-name securityaudituser
{
    "AttachedPolicies": [
        {
            "PolicyArn": "arn:aws:iam::aws:policy/SecurityAudit",
            "PolicyName": "SecurityAudit"
        }
    ]
}

aws iam list-user-policies --user-name securityaudituser
{
    "PolicyNames": []
}

aws iam list-attached-user-policies --user-name startrekbutter
{
    "AttachedPolicies": [
        {
            "PolicyName": "startrekbutter-bucket-s3-write-delete",
            "PolicyArn": "arn:aws:iam::159236164734:policy/startrekbutter-bucket-s3-write-delete"
        }
    ]
}

aws iam list-user-policies --user-name startrekbutter
{
    "PolicyNames": []
}

aws iam list-policies --query "Policies[?PolicyName == 'AWSSupportAccess']"
[
    {
        "AttachmentCount": 0,
        "Path": "/",
        "DefaultVersionId": "v1",
        "PermissionsBoundaryUsageCount": 0,
        "PolicyId": "ANPAJSNKQX2OW67GF4S7E",
        "IsAttachable": true,
        "Arn": "arn:aws:iam::aws:policy/AWSSupportAccess",
        "CreateDate": "2015-02-06T18:41:11Z",
        "PolicyName": "AWSSupportAccess",
        "UpdateDate": "2015-02-06T18:41:11Z"
    }
]

aws iam list-entities-for-policy --policy-arn arn:aws:iam::aws:policy/AWSSupportAccess
{
    "PolicyRoles": [],
    "PolicyGroups": [],
    "PolicyUsers": []
}

aws iam list-policies --only-attached --query "Policies[*].{Arn:Arn,Version:DefaultVersionId}" --output table
-------------------------------------------------------------------------------------------------------------------------------
|                                                        ListPolicies                                                         |
+------------------------------------------------------------------------------------------------------------------+----------+
|                                                        Arn                                                       | Version  |
+------------------------------------------------------------------------------------------------------------------+----------+
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaBasicExecutionRole-9ea2cfe2-b393-4c9e-a499-9041cbf7ffa0  |  v1      |
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaBasicExecutionRole-b8c05ebe-ee23-4300-ae76-cb07e735d432  |  v1      |
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaBasicExecutionRole-bb22c1bd-0be9-459d-8c7f-0aa1467b51db  |  v1      |
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaBasicExecutionRole-e4838aaf-3f8c-4867-b9f1-6f3067e0aad0  |  v1      |
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaBasicExecutionRole-fbc7e4bd-c463-40bc-913e-c02daa0d43ea  |  v1      |
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaKMSExecutionRole-342bcfbd-f81d-4a14-a7f2-55bb8a2615d9    |  v1      |
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaS3ExecutionRole-4284a89d-da79-4720-9194-f028e25defb0     |  v1      |
|  arn:aws:iam::159236164734:policy/service-role/AWSLambdaS3ExecutionRole-786835a2-f5f4-4008-b5cb-bc0efe5f5bc8     |  v2      |
|  arn:aws:iam::159236164734:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1      |  v1      |
|  arn:aws:iam::159236164734:policy/ctf3-challenge-s3-bucket-policy                                                |  v3      |
|  arn:aws:iam::159236164734:policy/startrekbutter-bucket-s3-write-delete                                          |  v1      |
|  arn:aws:iam::aws:policy/AmazonEC2FullAccess                                                                     |  v5      |
|  arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM                                                        |  v8      |
|  arn:aws:iam::aws:policy/IAMFullAccess                                                                           |  v2      |
|  arn:aws:iam::aws:policy/AmazonEKSClusterPolicy                                                                  |  v4      |
|  arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy                                                               |  v1      |
|  arn:aws:iam::aws:policy/aws-service-role/AutoScalingServiceRolePolicy                                           |  v2      |
|  arn:aws:iam::aws:policy/AmazonS3FullAccess                                                                      |  v1      |
|  arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly                                                      |  v3      |
|  arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess                                                                 |  v1      |
|  arn:aws:iam::aws:policy/aws-service-role/AWSElasticLoadBalancingServiceRolePolicy                               |  v6      |
|  arn:aws:iam::aws:policy/aws-service-role/AmazonRDSServiceRolePolicy                                             |  v7      |
|  arn:aws:iam::aws:policy/aws-service-role/AWSOrganizationsServiceTrustPolicy                                     |  v2      |
|  arn:aws:iam::aws:policy/service-role/AWSConfigRole                                                              |  v34     |
|  arn:aws:iam::aws:policy/AdministratorAccess                                                                     |  v1      |
|  arn:aws:iam::aws:policy/SecurityAudit                                                                           |  v32     |
|  arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess                                                                  |  v1      |
|  arn:aws:iam::aws:policy/IAMUserChangePassword                                                                   |  v2      |
|  arn:aws:iam::aws:policy/aws-service-role/AWSSupportServiceRolePolicy                                            |  v11     |
|  arn:aws:iam::aws:policy/AmazonSSMFullAccess                                                                     |  v4      |
|  arn:aws:iam::aws:policy/aws-service-role/AWSTrustedAdvisorServiceRolePolicy                                     |  v8      |
|  arn:aws:iam::aws:policy/aws-service-role/AmazonInspectorServiceRolePolicy                                       |  v4      |
|  arn:aws:iam::aws:policy/aws-service-role/APIGatewayServiceRolePolicy                                            |  v8      |
|  arn:aws:iam::aws:policy/aws-service-role/AWSConfigServiceRolePolicy                                             |  v20     |
|  arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy                                                                    |  v4      |
|  arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForAmazonEKSNodegroup                                    |  v4      |
|  arn:aws:iam::aws:policy/aws-service-role/AmazonEKSServiceRolePolicy                                             |  v2      |
+------------------------------------------------------------------------------------------------------------------+----------+

aws iam get-policy-version --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess" --version-id v1
{
    "PolicyVersion": {
        "CreateDate": "2015-02-06T18:39:46Z",
        "Document": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "*",
                    "Resource": "*",
                    "Effect": "Allow"
                }
            ]
        },
        "VersionId": "v1",
        "IsDefaultVersion": true
    }
}

aws iam list-entities-for-policy --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
{
    "PolicyRoles": [
        {
            "RoleId": "AROAICCX4KJROH7R4PIRI",
            "RoleName": "AccountAccess"
        },
        {
            "RoleId": "AROASKEZXBR7FW5OINE5T",
            "RoleName": "ec2acess"
        }
    ],
    "PolicyGroups": [
        {
            "GroupName": "SuperAdmins",
            "GroupId": "AGPAIXLYIGAZUGHNIGJK2"
        }
    ],
    "PolicyUsers": [
        {
            "UserId": "AIDAISKDJKT7RAWKJKS32",
            "UserName": "riyaz"
        },
        {
            "UserId": "AIDAJQ32PRSJPEU55L6NM",
            "UserName": "bharath"
        },
        {
            "UserId": "AIDASKEZXBR7EAGRBQT77",
            "UserName": "aws-training"
        }
    ]
}

aws configservice describe-configuration-recorders
{
    "ConfigurationRecorders": [
        {
            "recordingGroup": {
                "includeGlobalResourceTypes": false,
                "allSupported": true,
                "resourceTypes": []
            },
            "name": "default",
            "roleARN": "arn:aws:iam::159236164734:role/aws-service-role/config.amazonaws.com/AWSServiceRoleForConfig"
        }
    ]
}

aws ec2 describe-regions --output text --query 'Regions[*].[RegionName]'
eu-north-1
ap-south-1
eu-west-3
eu-west-2
eu-west-1
ap-northeast-2
ap-northeast-1
sa-east-1
ca-central-1
ap-southeast-1
ap-southeast-2
eu-central-1
us-east-1
us-east-2
us-west-1
us-west-2

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region eu-north-1

aws ec2 describe-iam-instance-profile-associations --output json --region eu-north-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region ap-south-1

aws ec2 describe-iam-instance-profile-associations --output json --region ap-south-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region eu-west-3

aws ec2 describe-iam-instance-profile-associations --output json --region eu-west-3
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region eu-west-2

aws ec2 describe-iam-instance-profile-associations --output json --region eu-west-2
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region eu-west-1

aws ec2 describe-iam-instance-profile-associations --output json --region eu-west-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region ap-northeast-2

aws ec2 describe-iam-instance-profile-associations --output json --region ap-northeast-2
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region ap-northeast-1

aws ec2 describe-iam-instance-profile-associations --output json --region ap-northeast-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region sa-east-1

aws ec2 describe-iam-instance-profile-associations --output json --region sa-east-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region ca-central-1

aws ec2 describe-iam-instance-profile-associations --output json --region ca-central-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region ap-southeast-1

aws ec2 describe-iam-instance-profile-associations --output json --region ap-southeast-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region ap-southeast-2

aws ec2 describe-iam-instance-profile-associations --output json --region ap-southeast-2
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region eu-central-1

aws ec2 describe-iam-instance-profile-associations --output json --region eu-central-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region us-east-1

aws ec2 describe-iam-instance-profile-associations --output json --region us-east-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region us-east-2

aws ec2 describe-iam-instance-profile-associations --output json --region us-east-2
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region us-west-1

aws ec2 describe-iam-instance-profile-associations --output json --region us-west-1
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region us-west-2

aws ec2 describe-iam-instance-profile-associations --output json --region us-west-2
{
    "IamInstanceProfileAssociations": []
}

aws ec2 describe-vpcs --query 'Vpcs[*].{VpcId: VpcId}' 
[
    {
        "VpcId": "vpc-8e1308f5"
    },
    {
        "VpcId": "vpc-06fe047dc51e37f50"
    },
    {
        "VpcId": "vpc-077be201d42566702"
    },
    {
        "VpcId": "vpc-aa6aabd1"
    },
    {
        "VpcId": "vpc-0007413db956d84cb"
    }
]

aws ec2 describe-route-tables --filter "Name=vpc-id,Values=vpc-8e1308f5" --query "RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}"
[
    {
        "RouteTableId": "rtb-ca2ff9b5",
        "AssociatedSubnets": [],
        "VpcId": "vpc-8e1308f5",
        "Routes": [
            {
                "Origin": "CreateRouteTable",
                "GatewayId": "local",
                "State": "active",
                "DestinationCidrBlock": "172.30.0.0/16"
            },
            {
                "Origin": "CreateRoute",
                "GatewayId": "igw-4ec69f36",
                "State": "active",
                "DestinationCidrBlock": "0.0.0.0/0"
            }
        ]
    }
]

aws ec2 describe-route-tables --filter "Name=vpc-id,Values=vpc-06fe047dc51e37f50" --query "RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}"
[
    {
        "AssociatedSubnets": [],
        "Routes": [
            {
                "DestinationCidrBlock": "172.30.0.0/16",
                "Origin": "CreateRouteTable",
                "State": "active",
                "GatewayId": "local"
            },
            {
                "DestinationCidrBlock": "0.0.0.0/0",
                "Origin": "CreateRoute",
                "State": "active",
                "GatewayId": "igw-0a6b7ba32c9e2df30"
            }
        ],
        "RouteTableId": "rtb-0e5651395a8563f1d",
        "VpcId": "vpc-06fe047dc51e37f50"
    }
]

aws ec2 describe-route-tables --filter "Name=vpc-id,Values=vpc-077be201d42566702" --query "RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}"
[
    {
        "Routes": [
            {
                "Origin": "CreateRouteTable",
                "DestinationCidrBlock": "10.0.0.0/24",
                "State": "active",
                "GatewayId": "local"
            }
        ],
        "VpcId": "vpc-077be201d42566702",
        "RouteTableId": "rtb-0462edf2af01ddbef",
        "AssociatedSubnets": []
    }
]

aws ec2 describe-route-tables --filter "Name=vpc-id,Values=vpc-aa6aabd1" --query "RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}"
[
    {
        "AssociatedSubnets": [],
        "VpcId": "vpc-aa6aabd1",
        "Routes": [
            {
                "Origin": "CreateRouteTable",
                "DestinationCidrBlock": "172.31.0.0/16",
                "State": "active",
                "GatewayId": "local"
            },
            {
                "Origin": "CreateRoute",
                "DestinationCidrBlock": "0.0.0.0/0",
                "State": "active",
                "GatewayId": "igw-0a5fb172"
            }
        ],
        "RouteTableId": "rtb-da648aa6"
    }
]

aws ec2 describe-route-tables --filter "Name=vpc-id,Values=vpc-0007413db956d84cb" --query "RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}"
[
    {
        "AssociatedSubnets": [],
        "Routes": [
            {
                "GatewayId": "local",
                "State": "active",
                "Origin": "CreateRouteTable",
                "DestinationCidrBlock": "10.0.0.0/16"
            },
            {
                "GatewayId": "igw-0bf739892eb857cb9",
                "State": "active",
                "Origin": "CreateRoute",
                "DestinationCidrBlock": "0.0.0.0/0"
            }
        ],
        "RouteTableId": "rtb-03793951b8d0dd448",
        "VpcId": "vpc-0007413db956d84cb"
    },
    {
        "AssociatedSubnets": [],
        "Routes": [
            {
                "GatewayId": "local",
                "State": "active",
                "Origin": "CreateRouteTable",
                "DestinationCidrBlock": "10.0.0.0/16"
            }
        ],
        "RouteTableId": "rtb-0b573f3625371e2de",
        "VpcId": "vpc-0007413db956d84cb"
    }
]

aws cloudtrail describe-trails
{
    "trailList": [
        {
            "IncludeGlobalServiceEvents": true,
            "S3BucketName": "ct-sumo-poc-5fd3e7169671782e",
            "TrailARN": "arn:aws:cloudtrail:ap-south-1:159236164734:trail/Sumo-Logic-POC",
            "Name": "Sumo-Logic-POC",
            "LogFileValidationEnabled": true,
            "HomeRegion": "ap-south-1",
            "IsMultiRegionTrail": true,
            "HasCustomEventSelectors": true
        }
    ]
}

aws cloudtrail get-event-selectors --trail-name arn:aws:cloudtrail:ap-south-1:159236164734:trail/Sumo-Logic-POC
{
    "TrailARN": "arn:aws:cloudtrail:ap-south-1:159236164734:trail/Sumo-Logic-POC",
    "EventSelectors": [
        {
            "DataResources": [
                {
                    "Type": "AWS::Lambda::Function",
                    "Values": [
                        "arn:aws:lambda"
                    ]
                }
            ],
            "IncludeManagementEvents": true,
            "ReadWriteType": "All"
        }
    ]
}

aws cloudtrail describe-trails --region ap-south-1
{
    "trailList": [
        {
            "Name": "Sumo-Logic-POC",
            "HomeRegion": "ap-south-1",
            "IncludeGlobalServiceEvents": true,
            "TrailARN": "arn:aws:cloudtrail:ap-south-1:159236164734:trail/Sumo-Logic-POC",
            "S3BucketName": "ct-sumo-poc-5fd3e7169671782e",
            "HasCustomEventSelectors": true,
            "IsMultiRegionTrail": true,
            "LogFileValidationEnabled": true
        }
    ]
}

aws cloudtrail describe-trails --query 'trailList[*].S3BucketName' --region ap-south-1
[
    "ct-sumo-poc-5fd3e7169671782e"
]

aws s3api get-bucket-acl --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AllUsers`]' --bucket ct-sumo-poc-5fd3e7169671782e
[]

aws s3api get-bucket-acl --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AuthenticatedUsers`]' --bucket ct-sumo-poc-5fd3e7169671782e
[]

aws s3api get-bucket-policy --bucket ct-sumo-poc-5fd3e7169671782e
{
    "Policy": "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Sid\":\"AWSCloudTrailAclCheck20150319\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"cloudtrail.amazonaws.com\"},\"Action\":\"s3:GetBucketAcl\",\"Resource\":\"arn:aws:s3:::ct-sumo-poc-5fd3e7169671782e\"},{\"Sid\":\"AWSCloudTrailWrite20150319\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"cloudtrail.amazonaws.com\"},\"Action\":\"s3:PutObject\",\"Resource\":\"arn:aws:s3:::ct-sumo-poc-5fd3e7169671782e/AWSLogs/159236164734/*\",\"Condition\":{\"StringEquals\":{\"s3:x-amz-acl\":\"bucket-owner-full-control\"}}}]}"
}

aws s3api get-bucket-logging --bucket ct-sumo-poc-5fd3e7169671782e
