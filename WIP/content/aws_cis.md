1	Identity and Access Management
1.1	Avoid the use of the "root" account (Scored)

aws iam generate-credential-report
aws iam get-credential-report --query 'Content' --output text | base64 -d | cut -d, -f1,5,11,16 | grep -B1 '<root_account>'

1.2	Ensure multi-factor authentication (MFA) is enabled for all IAM users that have a console password (Scored)

aws iam generate-credential-report
aws iam get-credential-report --query 'Content' --output text | base64 -d | cut -d, -f1,4,8

1.3	Ensure credentials unused for 90 days or greater are disabled (Scored)

aws iam generate-credential-report
aws iam get-credential-report --query 'Content' --output text | base64 -d | cut -d, -f1,4,5,6,9,10,11,14,15,16

1.4	Ensure access keys are rotated every 90 days or less (Scored)

aws iam generate-credential-report
aws iam get-credential-report --query 'Content' --output text | base64 -d

1.5	Ensure IAM password policy requires at least one uppercase letter (Scored)

aws iam get-account-password-policy

1.6	Ensure IAM password policy require at least one lowercase letter (Scored)

aws iam get-account-password-policy

1.7	Ensure IAM password policy require at least one symbol (Scored)

aws iam get-account-password-policy

1.8	Ensure IAM password policy require at least one number (Scored)

aws iam get-account-password-policy

1.9	Ensure IAM password policy requires minimum length of 14 or greater (Scored)

aws iam get-account-password-policy

1.10	Ensure IAM password policy prevents password reuse (Scored)

aws iam get-account-password-policy

1.11	Ensure IAM password policy expires passwords within 90 days or less (Scored)

aws iam get-account-password-policy

1.12	Ensure no root account access key exists (Scored)

aws iam get-credential-report --query 'Content' --output text | base64 -d |
cut -d, -f1,9,14 | grep -B1 '<root_account>'

1.13	Ensure MFA is enabled for the "root" account (Scored)

aws iam get-account-summary | grep "AccountMFAEnabled"

1.14	Ensure hardware MFA is enabled for the "root" account (Scored)

aws iam get-account-summary | grep "AccountMFAEnabled"
aws iam list-virtual-mfa-devices

1.15	Ensure security questions are registered in the AWS account (Not Scored)
1.16	Ensure IAM policies are attached only to groups or roles (Scored)

aws iam list-users --query 'Users[*].UserName'
aws iam list-attached-user-policies --user-name <iam_user>
aws iam list-user-policies --user-name <iam_user>

1.17	Maintain current contact details (Not Scored)
1.18	Ensure security contact information is registered (Not Scored)
1.19	Ensure IAM instance roles are used for AWS resource access from instances (Not Scored)

aws ec2 describe-regions --output text --query 'Regions[*].[RegionName]'
aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region <region-name>
aws ec2 describe-iam-instance-profile-associations --region <region-name> --output json

1.20	Ensure a support role has been created to manage incidents with AWS Support (Scored)

aws iam list-policies --query "Policies[?PolicyName == 'AWSSupportAccess']"
aws iam list-entities-for-policy --policy-arn arn:aws:iam::aws:policy/AWSSupportAccess

1.21	Do not setup access keys during initial user setup for all IAM users that have a console password (Not Scored)
1.22	Ensure IAM policies that allow full "*:*" administrative privileges are not created (Scored)

aws iam list-policies --only-attached --query "Policies[*].{Arn:Arn,Version:DefaultVersionId}" --output table
aws iam get-policy-version --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess" --version-id v1
aws iam list-entities-for-policy --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
 	 
2	Logging
2.1	Ensure CloudTrail is enabled in all regions (Scored)

aws cloudtrail describe-trails
aws cloudtrail describe-trails get-event-selectors --trail-name "<TRAIL_NAME_HERE>"

2.2	Ensure CloudTrail log file validation is enabled (Scored)

aws cloudtrail describe-trails –region <REGION-NAME>

2.3	Ensure the S3 bucket used to store CloudTrail logs is not publicly accessible (Scored)

aws cloudtrail describe-trails --query 'trailList[*].S3BucketName' --region <REGION-NAME>
aws s3api get-bucket-acl --bucket testcloudtrailzdfgb --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AllUsers` ]'
aws s3api get-bucket-acl --bucket testcloudtrailzdfgb --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AuthenticatedUsers` ]'
aws s3api get-bucket-policy --bucket testcloudtrailzdfgb

2.4	Ensure CloudTrail trails are integrated with CloudWatch Logs (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

2.5	Ensure AWS Config is enabled in all regions (Scored)

aws configservice describe-configuration-recorders

2.6	Ensure S3 bucket access logging is enabled on the CloudTrail S3 bucket (Scored)

aws cloudtrail describe-trails --query 'trailList[*].S3BucketName' --region <REGION-NAME>
aws s3api get-bucket-logging --bucket testcloudtrailzdfgb                                                                                             

2.7	Ensure CloudTrail logs are encrypted at rest using KMS CMKs (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

2.8	Ensure rotation for customer created CMKs is enabled (Scored)
2.9	Ensure VPC flow logging is enabled in all VPCs (Scored)
 	 
3	Monitoring
3.1	Ensure a log metric filter and alarm exist for unauthorized API calls (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.2	Ensure a log metric filter and alarm exist for Management Console sign-in without MFA (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.3	Ensure a log metric filter and alarm exist for usage of "root" account (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.4	Ensure a log metric filter and alarm exist for IAM policy changes (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.5	Ensure a log metric filter and alarm exist for CloudTrail configuration changes (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.6	Ensure a log metric filter and alarm exist for AWS Management Console authentication failures (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.7	Ensure a log metric filter and alarm exist for disabling or scheduled deletion of customer created CMKs (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.8	Ensure a log metric filter and alarm exist for S3 bucket policy changes (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.9	Ensure a log metric filter and alarm exist for AWS Config configuration changes (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.10	Ensure a log metric filter and alarm exist for security group changes (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.11	Ensure a log metric filter and alarm exist for changes to Network Access Control Lists (NACL) (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.12	Ensure a log metric filter and alarm exist for changes to network gateways (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.13	Ensure a log metric filter and alarm exist for route table changes (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>

3.14	Ensure a log metric filter and alarm exist for VPC changes (Scored)

aws cloudtrail describe-trails --region <REGION-NAME>
 	 
4	Networking
4.1	Ensure no security groups allow ingress from 0.0.0.0/0 to port 22 (Scored)
4.2	Ensure no security groups allow ingress from 0.0.0.0/0 to port 3389 (Scored)
4.3	Ensure the default security group of every VPC restricts all traffic (Scored)
4.4	Ensure routing tables for VPC peering are "least access" (Not Scored)

aws ec2 describe-vpcs --query 'Vpcs[*].{VpcId: VpcId}' 
aws ec2 describe-route-tables --filter "Name=vpc-id,Values=vpc-911fc7ea" --query "RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}"

## Annexure A – List of Users in AWS

1. username1
2. username2