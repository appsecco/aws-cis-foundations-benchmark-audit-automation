package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
)

const profile string = " --profile training"

const iam1 string = "aws iam generate-credential-report"
const iam2 string = "aws iam get-credential-report --query 'Content' --output text"
const iam2a string = " | base64 -d | cut -d, -f1,5,11,16 | grep -B1 '<root_account>'"
const iam2b string = " | base64 -d | cut -d, -f1,4,8"
const iam2c string = " | base64 -d | cut -d, -f1,4,5,6,9,10,11,14,15,16"
const iam2d string = " | base64 -d"
const iam6 string = "aws iam get-account-password-policy"
const iam8 string = "aws iam get-account-summary | grep \"AccountMFAEnabled\""
const iam9 string = "aws iam list-virtual-mfa-devices"
const iam10 string = "aws iam list-users --query 'Users[*].UserName'"
const iam11 string = "aws iam list-attached-user-policies --user-name <iam_user>"
const iam12 string = "aws iam list-user-policies --user-name <iam_user>"
const iam13 string = "aws iam list-policies --query \"Policies[?PolicyName == 'AWSSupportAccess']\""
const iam14 string = "aws iam list-entities-for-policy --policy-arn arn:aws:iam::aws:policy/AWSSupportAccess"
const iam15 string = "aws iam list-policies --only-attached --query \"Policies[*].{Arn:Arn,Version:DefaultVersionId}\" --output table"
const iam16 string = "aws iam get-policy-version --policy-arn \"arn:aws:iam::aws:policy/AdministratorAccess\" --version-id v1"
const iam17 string = "aws iam list-entities-for-policy --policy-arn \"arn:aws:iam::aws:policy/AdministratorAccess\""
const iam18 string = "aws configservice describe-configuration-recorders"
const ec2reg string = "aws ec2 describe-regions --output text --query 'Regions[*].[RegionName]'"
const ec2ins string = "aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --output text --region <region-name>"
const ec2iam string = "aws ec2 describe-iam-instance-profile-associations --region <region-name> --output json"
const ec2vpc string = "aws ec2 describe-vpcs --query 'Vpcs[*].{VpcId: VpcId}' "
const ec2route string = "aws ec2 describe-route-tables --filter \"Name=vpc-id,Values=vpc-911fc7ea\" --query \"RouteTables[*].{RouteTableId:RouteTableId, VpcId:VpcId, Routes:Routes, AssociatedSubnets:Associations[*].SubnetId}\""
const cloudtrail1 string = "aws cloudtrail describe-trails"
const cloudtrail2 string = "aws cloudtrail describe-trails get-event-selectors --trail-name \" < TRAIL_NAME_HERE > \""
const cloudtrail3 string = "aws cloudtrail describe-trails –region <REGION-NAME>"
const cloudtrail4 string = "aws cloudtrail describe-trails --query 'trailList[*].S3BucketName' --region <REGION-NAME>"
const cloudtrail5 string = "aws cloudtrail describe-trails --region <REGION-NAME>"
const cloudtrail6 string = "aws cloudtrail describe-trails --query 'trailList[*].S3BucketName' --region <REGION-NAME>"
const s3api1 string = "aws s3api get-bucket-acl --bucket testcloudtrailzdfgb --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AllUsers` ]'"
const s3api2 string = "aws s3api get-bucket-acl --bucket testcloudtrailzdfgb --query 'Grants[?Grantee.URI== `http://acs.amazonaws.com/groups/global/AuthenticatedUsers` ]'"
const s3api3 string = "aws s3api get-bucket-policy --bucket testcloudtrailzdfgb"
const s3api4 string = "aws s3api get-bucket-logging --bucket <BUCKET_NAME_HERE>"

var outfile, err = os.Create("")
var writer = bufio.NewWriter(outfile)

// func Test_IAM() {

// }

// func Test_Logging() {

// }

// func Test_Monitoring() {

// }

// func Test_Networking() {

// }

//RunCommand will run AWS commands
func RunCommand(command2run, profile string) {
	go io.Copy(writer, strings.NewReader("\n\n"+command2run+profile+"\n"))
	cmd := exec.Command("sh", "-c", command2run, profile)

	stdoutPipe, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	err = cmd.Start()
	if err != nil {
		panic(err)
	}

	go io.Copy(writer, stdoutPipe)
	cmd.Wait()
}

func main() {
	// open the out file for writing
	outfile, err = os.Create("./out.txt")
	if err != nil {
		panic(err)
	}
	defer outfile.Close()
	writer = bufio.NewWriter(outfile)
	defer writer.Flush()

	RunCommand(iam1, profile)
	RunCommand(iam2, profile)

}
